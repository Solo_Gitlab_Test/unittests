using AutoFixture;
using FluentAssertions;
using Moq;
using PaymentSystem.Engine.Common.AppArchitectureModel.Currencies;
using PaymentSystem.Engine.Common.AppArchitectureModel.ServiceProviders;
using PaymentSystem.Engine.Common.AppArchitectureModel.Tariffs;
using PaymentSystem.Engine.Common.DataContracts.Currencies;
using PaymentSystem.Engine.Common.DataContracts.ServiceProviders;
using PaymentSystem.Engine.Common.DataContracts.Tariffs;
using PaymentSystem.Engine.Common.Models;
using PaymentSystem.Engine.Core.AppArchitecture;
using PaymentSystem.Engine.Core.AppArchitecture.Tariffs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace PaymentSystem.Engine.Core.UnitTests.Tariffs
{
    #region SQLLite_TariffsDataSaveProductsTests_Fixture
    public class SQLLite_TariffsDataSaveProductsTests_Fixture : SQLiteFixtureBase
    {
        public override void SeedData(PaymentSystemEntitiesSecured con)
        {
            #region con.eTariffs.Add
            // eTariffs
            con.eTariffs.Add(new eTariff
            {
                ID = 1,
                CanBeProductsChargedDifferentCurrency = true,
                CanBeChargesChargedDifferentCurrency = true,
                IsActive = true,
                Name = "Tariff 01"
            });
            con.eTariffs.Add(new eTariff
            {
                ID = 2,
                CanBeProductsChargedDifferentCurrency = true,
                CanBeChargesChargedDifferentCurrency = true,
                IsActive = true,
                Name = "Tariff 02"
            });
            #endregion

            #region con.eProductImplementations.Add
            // eProductImplementations
            con.eProductImplementations.Add(new eProductImplementation
            {
                ID = 2
            });
            con.eProductImplementations.Add(new eProductImplementation
            {
                ID = 3
            });
            #endregion

            #region con.eTariffProducts.Add
            // eTariffProducts
            con.eTariffProducts.Add(new eTariffProduct
            {
                ID = 1,
                TariffID = 1,
                SetupCostText = "Product 1 SetupCost comment",
                IntervalCostText = "Product 1 IntervalCost comment"
            });
            con.eTariffProducts.Add(new eTariffProduct
            {
                ID = 2,
                TariffID = 1,
                SetupCostText = "Product 2 SetupCost comment",
                IntervalCostText = "Product 2 IntervalCost comment"
            });
            #endregion

            #region con.UnSecured.eClients.Add
            // eClients
            con.UnSecured.eClients.Add(new eClient // IsExchangeRateDependsOnWorkingTime
            {
                ID = 979978461,
                TariffID = 10,
                Tariff = new eTariff
                {
                    ID = 59,
                    CanBeProductsChargedDifferentCurrency = true,
                    CanBeChargesChargedDifferentCurrency = true,
                    IsExchangeRateDependsOnWorkingTime = true,
                    IsActive = true,
                    Name = "Tariff 02"
                }
            });
            #endregion

            #region con.eTariffCurrencies
            // eTariffCurrencies EUR-RUB IDs(1-5)
            // All to All
            con.eTariffCurrencies.Add(new eTariffCurrency { ID = 1, TariffID = 59, ClientID = null, CurrencyFromID = null, CurrencyToID = null, Margin = 1M, MarginOffTime = 2M, AmountFrom = 0M, IsPips = false, IsPipsOffTime = false });
            con.eTariffCurrencies.Add(new eTariffCurrency { ID = 2, TariffID = 59, ClientID = null, CurrencyFromID = null, CurrencyToID = null, Margin = 7M, MarginOffTime = 9M, AmountFrom = 300M, IsPips = false, IsPipsOffTime = false });
            con.eTariffCurrencies.Add(new eTariffCurrency { ID = 3, TariffID = 59, ClientID = null, CurrencyFromID = null, CurrencyToID = null, Margin = 7.5M, MarginOffTime = 9.35M, AmountFrom = 200M, IsPips = false, IsPipsOffTime = false });
            // Eur to All
            con.eTariffCurrencies.Add(new eTariffCurrency { ID = 4, TariffID = 59, ClientID = null, CurrencyFromID = 1, CurrencyToID = null, Margin = 5M, MarginOffTime = 10M, AmountFrom = 0M, IsPips = false, IsPipsOffTime = false });
            con.eTariffCurrencies.Add(new eTariffCurrency { ID = 5, TariffID = 59, ClientID = null, CurrencyFromID = 1, CurrencyToID = null, Margin = 2M, MarginOffTime = 3M, AmountFrom = 300M, IsPips = false, IsPipsOffTime = false });
            con.eTariffCurrencies.Add(new eTariffCurrency { ID = 6, TariffID = 59, ClientID = null, CurrencyFromID = 1, CurrencyToID = null, Margin = 17.22M, MarginOffTime = 28.33M, AmountFrom = 50M, IsPips = false, IsPipsOffTime = false });
            con.eTariffCurrencies.Add(new eTariffCurrency { ID = 7, TariffID = 59, ClientID = null, CurrencyFromID = 1, CurrencyToID = null, Margin = 9.1M, MarginOffTime = 10.55M, AmountFrom = 150M, IsPips = false, IsPipsOffTime = false });
            // Eur to Rub
            con.eTariffCurrencies.Add(new eTariffCurrency { ID = 8, TariffID = 59, ClientID = null, CurrencyFromID = 1, CurrencyToID = 5, Margin = 20M, MarginOffTime = 30M, AmountFrom = 0M, IsPips = false, IsPipsOffTime = false });
            con.eTariffCurrencies.Add(new eTariffCurrency { ID = 9, TariffID = 59, ClientID = null, CurrencyFromID = 1, CurrencyToID = 5, Margin = 15M, MarginOffTime = 25M, AmountFrom = 100M, IsPips = false, IsPipsOffTime = false });
            con.eTariffCurrencies.Add(new eTariffCurrency { ID = 10, TariffID = 59, ClientID = null, CurrencyFromID = 1, CurrencyToID = 5, Margin = 6M, MarginOffTime = 8M, AmountFrom = 300M, IsPips = false, IsPipsOffTime = false });
            #endregion

            #region con.eTariffServiceProviders.Add
            // eTariffServiceProviders
            con.eTariffServiceProviders.Add(new eTariffServiceProvider { ClientID = null, CurrencyID = 1, Fix = 0.0M, ID = 1, Max = 20.0M, Min = 10.0M, Percent = 10.0M, ServiceID = 6, TariffID = 1 });
            con.eTariffServiceProviders.Add(new eTariffServiceProvider { ClientID = null, CurrencyID = 1, Fix = 0.0M, ID = 2, Max = 0.0M, Min = 0.0M, Percent = 1.0M, ServiceID = 6, TariffID = 1 });
            #endregion

            #region con.eMT_Commissions.Add
            // eMT_Commissions
            con.eMT_Commissions.Add(new eMT_Commission
            {
                ClientID = null,
                CountriesKeyword = null,
                CountriesKeywordID = 4,
                CountryID = null,
                Created = new DateTime(2020, 11, 27, 10, 45, 55),
                Currency = null,
                CurrencyID = 1,
                Fix = null,
                From = 0M,
                ID = 55,
                ISOCountry = null,
                IsDeleted = false,
                MT_PaymentSystem = null,
                MT_PaymentSystemID = 2,
                Max = null,
                Min = null,
                PaymentType = PaymentSystem.Engine.Common.DataContracts.Enums.MTPaymentTypes.LOCAL_PICK_UP,
                PercentValue = 2M,
                TariffID = 1,
                Updated = new DateTime(2021, 9, 27, 21, 4, 2)
            });

            con.eMT_Commissions.Add(new eMT_Commission
            {
                ClientID = null,
                CountriesKeyword = null,
                CountriesKeywordID = 4,
                CountryID = null,
                Created = new DateTime(2020, 11, 27, 10, 45, 55),
                Currency = null,
                CurrencyID = 1,
                Fix = null,
                From = 0.0M,
                ID = 56,
                ISOCountry = null,
                IsDeleted = false,
                MT_PaymentSystem = null,
                MT_PaymentSystemID = 2,
                Max = null,
                Min = null,
                PaymentType = PaymentSystem.Engine.Common.DataContracts.Enums.MTPaymentTypes.BANK_PERSON,
                PercentValue = 2M,
                TariffID = 1,
                Updated = new DateTime(2021, 9, 27, 21, 04, 02)
            });
            #endregion

            #region con.eTariffCharges.Add
            con.eTariffCharges.Add(new eTariffCharge
            {
                BusinessCategoryID = null,
                ChargesTypeID = 1,
                ClientID = null,
                CurrencyID = 1,
                Fix = 100M,
                ID = 708,
                IsDefaultCurrency = false,
                TariffID = 1
            });

            con.eTariffCharges.Add(new eTariffCharge
            {
                BusinessCategoryID = null,
                ChargesTypeID = 6,
                ClientID = null,
                CurrencyID = 1,
                Fix = 0M,
                ID = 710,
                IsDefaultCurrency = false,
                TariffID = 1
            });
            #endregion

            #region con.eTariffBankCurrencies.Add
            con.eTariffBankCurrencies.Add(new eTariffBankCurrency
            {
                CurrencyID = 2,
                ID = 42,
                IncomingAsCurrencyID = 1,
                IsActive = false,
                OutgoingAsCurrencyID = 1,
                SHACountriesJSON = null,
                TariffID = 1,
            });
            con.eTariffBankCurrencies.Add(new eTariffBankCurrency
            {
                CurrencyID = 3,
                ID = 43,
                IncomingAsCurrencyID = null,
                IsActive = false,
                OutgoingAsCurrencyID = null,
                SHACountriesJSON = null,
                TariffID = 1
            });
            #endregion

            #region con.eTariffBankSettings.Add
            // inward
            con.eTariffBankSettings.Add(new eTariffBankSetting
            {
                BusinessCategoryID = null,
                Charges = null,
                ClientID = null,
                CurrencyID = null,
                Fix = 0M,
                From = 0M,
                ID = 537,
                IsIncoming = true,
                Max = 0M,
                Min = 0M,
                Percent = 0M,
                Priority = PaymentSystem.Engine.Common.DataContracts.Enums.BankWirePriority.SWIFT,
                TariffBankCurrencyID = 42,
                WireTransferSystemID = 2
            });
            con.eTariffBankSettings.Add(new eTariffBankSetting
            {
                BusinessCategoryID = null,
                Charges = null,
                ClientID = null,
                CurrencyID = null,
                Fix = 0M,
                From = 0M,
                ID = 552,
                IsIncoming = true,
                Max = 0M,
                Min = 0M,
                Percent = 0M,
                Priority = PaymentSystem.Engine.Common.DataContracts.Enums.BankWirePriority.SWIFT,
                TariffBankCurrencyID = 43,
                WireTransferSystemID = 2
            });
            // outward
            con.eTariffBankSettings.Add(new eTariffBankSetting
            {
                BusinessCategoryID = null,
                Charges = PaymentSystem.Engine.Common.DataContracts.Enums.BankWireChargeTypes.SHA,
                ClientID = null,
                CurrencyID = null,
                Fix = 75M,
                From = 0M,
                ID = 546,
                IsIncoming = false,
                Max = 0M,
                Min = 0M,
                Percent = 0M,
                Priority = PaymentSystem.Engine.Common.DataContracts.Enums.BankWirePriority.SWIFT,
                TariffBankCurrencyID = 42,
                WireTransferSystemID = 2
            });
            con.eTariffBankSettings.Add(new eTariffBankSetting
            {
                BusinessCategoryID = null,
                Charges = PaymentSystem.Engine.Common.DataContracts.Enums.BankWireChargeTypes.OUR,
                ClientID = null,
                CurrencyID = null,
                Fix = 50M,
                From = 0M,
                ID = 16,
                IsIncoming = false,
                Max = 0M,
                Min = 0M,
                Percent = 0M,
                Priority = PaymentSystem.Engine.Common.DataContracts.Enums.BankWirePriority.SWIFT,
                TariffBankCurrencyID = 43,
                WireTransferSystemID = 3
            });
            #endregion

        }

        public override void SetupFixture(IFixture fix)
        {
            // Mock ICurrenciesService
            var mockCurrenciesService = fix.Freeze<Mock<ICurrenciesService>>();
            Dictionary<short, Currency> Currencies = new Dictionary<short, Currency>()
            {
                {1, new Currency() { ID = 1, CurrCodeInt = "978", CurrCodeChr = "EUR"} },
                {2, new Currency() { ID = 2, CurrCodeInt = "840", CurrCodeChr = "USD"} },
                {3, new Currency() { ID = 3, CurrCodeInt = "826", CurrCodeChr = "GBP"} },
            };
            mockCurrenciesService.Setup(x => x.Currencies).Returns(Currencies);


            // Mock ICurrencyRatesService
            var mockCurrencyRatesService = fix.Freeze<Mock<ICurrencyRatesService>>();
            mockCurrencyRatesService.Setup(x => x.GetExchangeRate_Base(It.IsAny<short>(), It.IsAny<short>(), It.IsAny<bool>())).Returns(2);


            // Mock IServiceProvidersService
            var mockServiceProvidersService = fix.Freeze<Mock<IServiceProvidersService>>();
            List<ServiceProvider_Service> serviceProviders = new List<ServiceProvider_Service>
            {
                new ServiceProvider_Service { ID = 1},
                new ServiceProvider_Service { ID = 2},
                new ServiceProvider_Service { ID = 3},
                new ServiceProvider_Service { ID = 4},
                new ServiceProvider_Service { ID = 5}
            };
            mockServiceProvidersService.Setup(x => x.Services_GetAll(It.IsAny<ServiceFilter>())).Returns(serviceProviders);


            // Mock ITariffSettingsService
            var mockTariffSettingsService = fix.Freeze<Mock<ITariffSettingsService>>();
            List<TariffChargesType> tariffSettingsService = new List<TariffChargesType>
            {
                new TariffChargesType { ID = 1},
                new TariffChargesType { ID = 2},
                new TariffChargesType { ID = 3}
            };
            mockTariffSettingsService.Setup(x => x.TariffChargesTypes_GetAll()).Returns(tariffSettingsService);
        }

    }
    #endregion

    public class TariffHelperTariffsData_Save_Products_Tests : IClassFixture<SQLLite_TariffsDataSaveProductsTests_Fixture>
    {
        private readonly SQLLite_TariffsDataSaveProductsTests_Fixture fixture;

        public TariffHelperTariffsData_Save_Products_Tests(SQLLite_TariffsDataSaveProductsTests_Fixture fixture)
        {
            this.fixture = fixture;
        }

        // DATA

        #region GetTariffProductsDataSave        
        public class GetTariffProductsDataSave : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    new TariffsData()
                    {
                        Tariffs = new List<Tariff>()
                        {
                            new Tariff()
                            {
                                ID = 1,
                                Products = new List<TariffProduct>()
                                {
                                    new TariffProduct()
                                    {
                                        ID = 0,
                                        TariffID = 1,
                                        CurrencyID = 3,
                                        ClientID = null,
                                        ProductID = 3,
                                        SetupCost = 5M,
                                        TrialIntervalCost = 0M,
                                        TrialIntervalLength = 0,
                                        TrialIntervalType = 0,
                                        RegularIntervalCost = 25M,
                                        RegularIntervalType = 0,
                                        MaxTrialTicks = 0,
                                        RegularIntervalLength = 1,
                                        SetupCostText = "SetupCost comment 3",
                                        IntervalCostText = "IntervalCost comment 3",
                                        TrialCostText = String.Empty,
                                        ProductOrderID = null
                                    }
                                }
                            }
                        },
                        ChargesTypes = new List<TariffChargesType>()
                    }
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
        #endregion

        #region GetTariffServiceProviderDataSave
        public class GetTariffServiceProviderDataSave : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    new TariffsData()
                    {
                        Tariffs = new List<Tariff>()
                        {
                            new Tariff()
                            {
                                ID = 1,
                                ServiceProviders = new List<TariffServiceProvider>
                                {
                                    new TariffServiceProvider
                                    {
                                        ClientID = null,
                                        CurrencyID = 1,
                                        Fix = 60.0M,
                                        ID = 0,
                                        Max = 50.0M,
                                        Min = 20.0M,
                                        Percent = 5.0M,
                                        ServiceID = 5,
                                        TariffID = 1
                                    }
                                }
                            }
                        },
                        ChargesTypes = new List<TariffChargesType>()
                    }
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        #endregion

        #region GetTariffExchangesDataSave
        public class GetTariffExchangesDataSave : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    new TariffsData()
                    {
                        Tariffs = new List<Tariff>()
                        {
                            new Tariff()
                            {
                                ID = 1,
                                Exchanges = new List<TariffCurrency>
                                {
                                    new TariffCurrency
                                    {
                                        AmountFrom = 0M,
                                        ClientID = null,
                                        CurrencyFromID = 1,
                                        CurrencyToID = null,
                                        ID = 0,
                                        IsFromPersonalTariff = false,
                                        IsPips = false,
                                        IsPipsOffTime = false,
                                        Margin = 7M,
                                        MarginOffTime = 7M,
                                        TariffID = 1
                                    }
                                }
                            }
                        },
                        ChargesTypes = new List<TariffChargesType>()
                    }
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        #endregion

        #region GetMT_CommissionsDataSave
        public class GetMT_CommissionsDataSave : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    new TariffsData()
                    {
                        Tariffs = new List<Tariff>()
                        {
                            new Tariff()
                            {
                                ID = 1,
                                Commissions = new List<Common.DataContracts.MT.MT_Commission>
                                {
                                    new Common.DataContracts.MT.MT_Commission
                                    {
                                        ClientID = null,
                                        CountriesKeyword = null,
                                        CountriesKeywordID = 6,
                                        CountryID =  null,
                                        Created = DateTime.MaxValue,
                                        Currency = null,
                                        CurrencyID = 1,
                                        Fix = 5.0M,
                                        From = 100.0M,
                                        ID = 0,
                                        ISOCountry = null,
                                        IsDeleted = false,
                                        IsFromPersonalTariff = false,
                                        MT_PaymentSystem = null,
                                        MT_PaymentSystemID = 2,
                                        Max = 8.0M,
                                        Min = 7.0M,
                                        PaymentType = PaymentSystem.Engine.Common.DataContracts.Enums.MTPaymentTypes.CARD,
                                        PercentValue = 2.0M,
                                        TariffID = 1,
                                        Updated = null
                                    }
                                }
                            }
                        },
                        ChargesTypes = new List<TariffChargesType>()
                    }
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
        #endregion

        #region GetTariffChargesDataSave
        public class GetTariffChargesDataSave : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    new TariffsData()
                    {
                        Tariffs = new List<Tariff>()
                        {
                            new Tariff()
                            {
                                ID = 1,
                                Charges = new List<TariffCharge>
                                {
                                    new TariffCharge
                                    {
                                        BusinessCategoryID = null,
                                        ChargesTypeID = 1,
                                        ClientID = null,
                                        CurrencyID = 1,
                                        Fix = 700M,
                                        ID = 0,
                                        IsDefaultCurrency = false,
                                        IsFromPersonalTariff = false
                                    }
                                }
                            }
                        },
                        ChargesTypes = new List<TariffChargesType>()
                    }
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        #endregion

        #region TariffBankCurrenciesDataSave
        public class TariffBankCurrenciesDataSave : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    new TariffsData()
                    {
                        Tariffs = new List<Tariff>()
                        {
                            new Tariff()
                            {
                                ID = 1,
                                BankCurrencies = new List<TariffBankCurrency>
                                {
                                    new TariffBankCurrency
                                    {
                                        CurrencyID = 1,
                                        ID = 0,
                                        IncomingAsCurrencyID = null,
                                        Inward = new List<TariffBankSetting>
                                        {
                                            new TariffBankSetting
                                            {
                                                BusinessCategoryID = null,
                                                Charges = null,
                                                ClientID = null,
                                                CurrencyID = null,
                                                Fix = 0M,
                                                From = 0M,
                                                ID = 0,
                                                IsFromPersonalTariff = false,
                                                IsIncoming = true,
                                                Max = 0M,
                                                Min = 0M,
                                                Percent = 0M,
                                                Priority = PaymentSystem.Engine.Common.DataContracts.Enums.BankWirePriority.SWIFT,
                                                TariffBankCurrencyID = 0,
                                                WireTransferSystemID = 2
                                            }
                                        },
                                        IsActive = false,
                                        IsFromPersonalTariff = false,
                                        OutgoingAsCurrencyID = null,
                                        Outward = new List<TariffBankSetting>
                                        {
                                            new TariffBankSetting
                                            {
                                                BusinessCategoryID = null,
                                                Charges = PaymentSystem.Engine.Common.DataContracts.Enums.BankWireChargeTypes.OUR,
                                                ClientID = null,
                                                CurrencyID = null,
                                                Fix = 10M,
                                                From = 0M,
                                                ID = 0,
                                                IsFromPersonalTariff = false,
                                                IsIncoming = false,
                                                Max = 0M,
                                                Min = 0M,
                                                Percent = 0M,
                                                Priority = PaymentSystem.Engine.Common.DataContracts.Enums.BankWirePriority.SWIFT,
                                                TariffBankCurrencyID = 0,
                                                WireTransferSystemID = 1
                                            }
                                        },
                                        SHACountries = new List<Common.DataContracts.Enums.Countries>
                                        {
                                            Common.DataContracts.Enums.Countries.Bahrain,
                                            Common.DataContracts.Enums.Countries.Russia,
                                            Common.DataContracts.Enums.Countries.United_States,
                                            Common.DataContracts.Enums.Countries.Germany

                                        },
                                        TariffID = 1
                                    }
                                }
                            }
                        },
                        ChargesTypes = new List<TariffChargesType>()
                    }
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        #endregion


        // TESTS

        #region TariffBankCurrencies save/update/delete
        [Theory]
        [ClassData(typeof(TariffBankCurrenciesDataSave))]
        public void TariffBankCurrenciesSaveTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();


                    // act
                    service.TariffsData_Save(td);
                    var newTariffBankCurrencyID = con.eTariffBankCurrencies.Max(x => x.ID);


                    // assert
                    // TariffBankCurrencies
                    var resultTariffBankCurrencyToBeSaved = con.eTariffBankCurrencies.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.CurrencyID == td.Tariffs[0].BankCurrencies[0].CurrencyID && x.IsActive == td.Tariffs[0].BankCurrencies[0].IsActive);
                    resultTariffBankCurrencyToBeSaved.Should().NotBeNull();

                    // eTariffBankSettings inward
                    var resultTariffBankSettingsInwardToBeSaved = con.eTariffBankSettings.FirstOrDefault(x => x.TariffBankCurrencyID == newTariffBankCurrencyID && x.IsIncoming == td.Tariffs[0].BankCurrencies[0].Inward[0].IsIncoming && x.Max == td.Tariffs[0].BankCurrencies[0].Inward[0].Max);
                    resultTariffBankSettingsInwardToBeSaved.Should().NotBeNull();

                    // eTariffBankSettings outward
                    var resultTariffBankSettingsOutwardToBeSaved = con.eTariffBankSettings.FirstOrDefault(x => x.TariffBankCurrencyID == newTariffBankCurrencyID && x.IsIncoming == td.Tariffs[0].BankCurrencies[0].Outward[0].IsIncoming && x.Max == td.Tariffs[0].BankCurrencies[0].Inward[0].Max);
                    resultTariffBankSettingsOutwardToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(TariffBankCurrenciesDataSave))]
        public void TariffBankCurrenciesUpdateTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();
                    // add new TariffBankCurrency
                    service.TariffsData_Save(td);
                    // get the id of the last added TariffBankCurrency && TariffBankSettings (inward + outward)
                    var newTariffBankCurrencyID = con.eTariffBankCurrencies.Max(x => x.ID);
                    var newTariffBankSettingsInwardID = con.eTariffBankSettings.Where(x => x.IsIncoming == true).Max(x => x.ID);
                    var newTariffBankSettingsOutwardID = con.eTariffBankSettings.Where(x => x.IsIncoming == false).Max(x => x.ID);
                    td.Tariffs[0].BankCurrencies[0].ID = newTariffBankCurrencyID;
                    td.Tariffs[0].BankCurrencies[0].Inward[0].ID = newTariffBankSettingsInwardID;
                    td.Tariffs[0].BankCurrencies[0].Outward[0].ID = newTariffBankSettingsOutwardID;
                    td.Tariffs[0].BankCurrencies[0].Inward[0].TariffBankCurrencyID = newTariffBankCurrencyID;
                    td.Tariffs[0].BankCurrencies[0].Outward[0].TariffBankCurrencyID = newTariffBankCurrencyID;

                    // great new value of the field (for last added TariffBankCurrency && TariffBankSettings (inward + outward))
                    td.Tariffs[0].BankCurrencies[0].IsActive = false;
                    td.Tariffs[0].BankCurrencies[0].Inward[0].Max = 999M;
                    td.Tariffs[0].BankCurrencies[0].Outward[0].Max = 888M;


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    // TariffBankCurrencies
                    var resultTariffBankCurrencyToBeSaved = con.eTariffBankCurrencies.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.CurrencyID == td.Tariffs[0].BankCurrencies[0].CurrencyID && x.IsActive == td.Tariffs[0].BankCurrencies[0].IsActive);
                    resultTariffBankCurrencyToBeSaved.Should().NotBeNull();

                    // eTariffBankSettings inward
                    var resultTariffBankSettingsInwardToBeSaved = con.eTariffBankSettings.FirstOrDefault(x => x.TariffBankCurrencyID == newTariffBankCurrencyID && x.IsIncoming == td.Tariffs[0].BankCurrencies[0].Inward[0].IsIncoming && x.Max == td.Tariffs[0].BankCurrencies[0].Inward[0].Max);
                    resultTariffBankSettingsInwardToBeSaved.Should().NotBeNull();

                    // eTariffBankSettings outward
                    var resultTariffBankSettingsOutwardToBeSaved = con.eTariffBankSettings.FirstOrDefault(x => x.TariffBankCurrencyID == newTariffBankCurrencyID && x.IsIncoming == td.Tariffs[0].BankCurrencies[0].Outward[0].IsIncoming && x.Max == td.Tariffs[0].BankCurrencies[0].Outward[0].Max);
                    resultTariffBankSettingsOutwardToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(TariffBankCurrenciesDataSave))]
        public void TariffBankCurrenciesDeleteTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();
                    short newTariffBankCurrencyToBeRemovedID = (short)(con.eTariffBankCurrencies.Max(x => x.ID) + 1);
                    var newTariffBankSettingsInwardToBeRemovedID = con.eTariffBankSettings.Max(x => x.ID) + 1;
                    var newTariffBankSettingsOutwardToBeRemovedID = newTariffBankSettingsInwardToBeRemovedID + 1;

                    #region add eTariffBankCurrencies & eTariffBankSettings (Inward + Outward)
                    con.eTariffBankCurrencies.Add(new eTariffBankCurrency
                    {
                        CurrencyID = 2,
                        ID = newTariffBankCurrencyToBeRemovedID,
                        IncomingAsCurrencyID = 1,
                        IsActive = false,
                        OutgoingAsCurrencyID = 1,
                        SHACountriesJSON = null,
                        TariffID = 1,
                    });
                    con.eTariffBankSettings.Add(new eTariffBankSetting
                    {
                        BusinessCategoryID = null,
                        Charges = null,
                        ClientID = null,
                        CurrencyID = null,
                        Fix = 0M,
                        From = 0M,
                        ID = newTariffBankSettingsInwardToBeRemovedID,
                        IsIncoming = true,
                        Max = 0M,
                        Min = 0M,
                        Percent = 0M,
                        Priority = PaymentSystem.Engine.Common.DataContracts.Enums.BankWirePriority.SWIFT,
                        TariffBankCurrencyID = newTariffBankCurrencyToBeRemovedID,
                        WireTransferSystemID = 2
                    });
                    con.eTariffBankSettings.Add(new eTariffBankSetting
                    {
                        BusinessCategoryID = null,
                        Charges = PaymentSystem.Engine.Common.DataContracts.Enums.BankWireChargeTypes.SHA,
                        ClientID = null,
                        CurrencyID = null,
                        Fix = 75M,
                        From = 0M,
                        ID = newTariffBankSettingsOutwardToBeRemovedID,
                        IsIncoming = false,
                        Max = 0M,
                        Min = 0M,
                        Percent = 0M,
                        Priority = PaymentSystem.Engine.Common.DataContracts.Enums.BankWirePriority.SWIFT,
                        TariffBankCurrencyID = newTariffBankCurrencyToBeRemovedID,
                        WireTransferSystemID = 2
                    });
                    #endregion

                    con.SaveChanges();


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultTariffBankCurrencyToBeRemoved = con.eTariffBankCurrencies.FirstOrDefault(x => x.ID == newTariffBankCurrencyToBeRemovedID);
                    resultTariffBankCurrencyToBeRemoved.Should().BeNull();
                    var resultTariffBankSettingsInwardToBeRemoved = con.eTariffBankSettings.FirstOrDefault(x => x.ID == newTariffBankSettingsInwardToBeRemovedID && x.IsIncoming == true);
                    resultTariffBankSettingsInwardToBeRemoved.Should().BeNull();
                    var resultTariffBankSettingsOutwardToBeRemoved = con.eTariffBankSettings.FirstOrDefault(x => x.ID == newTariffBankSettingsOutwardToBeRemovedID && x.IsIncoming == false);
                    resultTariffBankSettingsOutwardToBeRemoved.Should().BeNull();

                    var resultTariffBankCurrencyToBeSaved = con.eTariffBankCurrencies.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.IsActive == td.Tariffs[0].BankCurrencies[0].IsActive);
                    resultTariffBankCurrencyToBeSaved.Should().NotBeNull();
                    var resultTariffBankSettingsInwardToBeSaved = con.eTariffBankSettings.FirstOrDefault(x => x.IsIncoming == true && x.Fix == td.Tariffs[0].BankCurrencies[0].Inward[0].Fix);
                    resultTariffBankSettingsInwardToBeSaved.Should().NotBeNull();
                    var resultTariffBankSettingsOutwardToBeSaved = con.eTariffBankSettings.FirstOrDefault(x => x.IsIncoming == false && x.Fix == td.Tariffs[0].BankCurrencies[0].Outward[0].Fix);
                    resultTariffBankSettingsOutwardToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        #endregion

        #region TariffCharges save/update/delete
        [Theory]
        [ClassData(typeof(GetTariffChargesDataSave))]
        public void TariffChargesSaveTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultTariffChargeToBeSaved = con.eTariffCharges.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.Fix == td.Tariffs[0].Charges[0].Fix);
                    resultTariffChargeToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetTariffChargesDataSave))]
        public void TariffChargesUpdateTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();
                    // add new
                    service.TariffsData_Save(td);
                    // get the id of the last added
                    var newTariffChargeID = con.eTariffCharges.Max(x => x.ID);
                    td.Tariffs[0].Charges[0].ID = newTariffChargeID;
                    // great new value of the field
                    td.Tariffs[0].Charges[0].Fix = 333M;


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultTariffChargeToBeUpdated = con.eTariffCharges.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.Fix == td.Tariffs[0].Charges[0].Fix);
                    resultTariffChargeToBeUpdated.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetTariffChargesDataSave))]
        public void TariffChargesDeleteTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();
                    int TariffChargeToBeRemovedID = con.eTariffCharges.Max(x => x.ID) + 1;

                    eTariffCharge eTariffChargesToBeRemoved = new eTariffCharge
                    {
                        BusinessCategoryID = null,
                        ChargesTypeID = 1,
                        ClientID = null,
                        CurrencyID = 1,
                        Fix = 444M,
                        ID = TariffChargeToBeRemovedID,
                        IsDefaultCurrency = false,
                        TariffID = 1
                    };


                    con.eTariffCharges.Add(eTariffChargesToBeRemoved);
                    con.SaveChanges();


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultTariffChargeToBeRemoved = con.eTariffCharges.FirstOrDefault(x => x.ID == TariffChargeToBeRemovedID);
                    resultTariffChargeToBeRemoved.Should().BeNull();

                    var resultTariffChargeToBeSaved = con.eTariffCharges.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.Fix == td.Tariffs[0].Charges[0].Fix);
                    resultTariffChargeToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }


        #endregion

        #region TariffCommissions save/update/delete
        [Theory]
        [ClassData(typeof(GetMT_CommissionsDataSave))]
        public void TariffCommissionsSaveTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultCommissionToBeSaved = con.eMT_Commissions.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.From == td.Tariffs[0].Commissions[0].From);
                    resultCommissionToBeSaved.Should().NotBeNull();
                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetMT_CommissionsDataSave))]
        public void TariffCommissionsUpdateTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();
                    // add new
                    service.TariffsData_Save(td);
                    // get the id of the last added
                    var newCommissionID = con.eMT_Commissions.Max(x => x.ID);
                    td.Tariffs[0].Commissions[0].ID = newCommissionID;
                    // great new value of the field
                    td.Tariffs[0].Commissions[0].From = 500M;


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultCommissionToBeUpdated = con.eMT_Commissions.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.From == td.Tariffs[0].Commissions[0].From);
                    resultCommissionToBeUpdated.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetMT_CommissionsDataSave))]
        public void TariffCommissionsDeleteTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();
                    int TariffCommissionToBeRemovedID = con.eMT_Commissions.Max(x => x.ID) + 1;

                    eMT_Commission TariffCommissionToBeRemoved = new eMT_Commission
                    {
                        ClientID = null,
                        CountriesKeyword = null,
                        CountriesKeywordID = 4,
                        CountryID = null,
                        Created = new DateTime(2020, 11, 27, 10, 45, 55),
                        Currency = null,
                        CurrencyID = 1,
                        Fix = 255,
                        From = 0.0M,
                        ID = TariffCommissionToBeRemovedID,
                        ISOCountry = null,
                        IsDeleted = false,
                        MT_PaymentSystem = null,
                        MT_PaymentSystemID = 1,
                        Max = 100,
                        Min = 50,
                        PaymentType = PaymentSystem.Engine.Common.DataContracts.Enums.MTPaymentTypes.CARD,
                        PercentValue = 2M,
                        TariffID = 1,
                        Updated = new DateTime(2021, 9, 27, 21, 04, 02)
                    };

                    con.eMT_Commissions.Add(TariffCommissionToBeRemoved);
                    con.SaveChanges();


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultTariffCommissionToBeRemoved = con.eMT_Commissions.FirstOrDefault(x => x.ID == TariffCommissionToBeRemovedID);
                    resultTariffCommissionToBeRemoved.Should().BeNull();

                    var resultTariffCommissionToBeSaved = con.eMT_Commissions.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.Fix == td.Tariffs[0].Commissions[0].Fix);
                    resultTariffCommissionToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }
        #endregion

        #region TariffServiceProviders save/update/delete
        [Theory]
        [ClassData(typeof(GetTariffServiceProviderDataSave))]
        public void TariffServiceProvidersSaveTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultServiceProviderToBeSaved = con.eTariffServiceProviders.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.Fix == td.Tariffs[0].ServiceProviders[0].Fix);
                    resultServiceProviderToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetTariffServiceProviderDataSave))]
        public void TariffServiceProvidersUpdateTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();
                    // add new
                    service.TariffsData_Save(td);
                    // get the id of the last added
                    var newTariffServiceProvidersID = con.eTariffServiceProviders.Max(x => x.ID);
                    td.Tariffs[0].ServiceProviders[0].ID = newTariffServiceProvidersID;
                    // great new value of the field
                    td.Tariffs[0].ServiceProviders[0].Fix = 100M;


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultServiceProviderToBeUpdated = con.eTariffServiceProviders.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.Fix == td.Tariffs[0].ServiceProviders[0].Fix);
                    resultServiceProviderToBeUpdated.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetTariffServiceProviderDataSave))]
        public void TariffServiceProvidersDeleteTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();
                    int TariffServiceProvidersToBeRemovedID = con.eTariffServiceProviders.Max(x => x.ID) + 1;

                    eTariffServiceProvider serviceProviderToBeRemoved = new eTariffServiceProvider
                    {
                        ClientID = null,
                        CurrencyID = 1,
                        Fix = 60.0M,
                        ID = TariffServiceProvidersToBeRemovedID,
                        Max = 50.0M,
                        Min = 20.0M,
                        Percent = 5.0M,
                        ServiceID = 5,
                        TariffID = 1
                    };

                    con.eTariffServiceProviders.Add(serviceProviderToBeRemoved);
                    con.SaveChanges();

                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultServiceProviderToBeRemoved = con.eTariffServiceProviders.FirstOrDefault(x => x.ID == TariffServiceProvidersToBeRemovedID);
                    resultServiceProviderToBeRemoved.Should().BeNull();

                    var resultServiceProviderToBeSaved = con.eTariffServiceProviders.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.Fix == td.Tariffs[0].ServiceProviders[0].Fix);
                    resultServiceProviderToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        #endregion

        #region TariffExchanges save/update/delete
        [Theory]
        [ClassData(typeof(GetTariffExchangesDataSave))]
        public void TariffExchangesSaveTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultTariffCurrency = con.eTariffCurrencies.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID);
                    resultTariffCurrency.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetTariffExchangesDataSave))]
        public void TariffExchangesUpdateTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();
                    // add new
                    service.TariffsData_Save(td);
                    // get the id of the last added
                    var newTariffCurrencyID = con.eTariffCurrencies.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID).ID;
                    td.Tariffs[0].Exchanges[0].ID = newTariffCurrencyID;
                    // great new value of the field
                    td.Tariffs[0].Exchanges[0].MarginOffTime = 70M;


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultTariffCurrency = con.eTariffCurrencies.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.MarginOffTime == td.Tariffs[0].Exchanges[0].MarginOffTime);
                    resultTariffCurrency.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetTariffExchangesDataSave))]
        public void TariffExchangesDeleteTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();
                    int tariffCurrenciesToBeRemovedID = con.eTariffCurrencies.Max(x => x.ID) + 1;

                    eTariffCurrency tariffCurrenciesToBeRemoved = new eTariffCurrency
                    {
                        ID = tariffCurrenciesToBeRemovedID,
                        TariffID = 1,
                        ClientID = null,
                        CurrencyFromID = null,
                        CurrencyToID = null,
                        Margin = 1M,
                        MarginOffTime = 2M,
                        AmountFrom = 0M,
                        IsPips = false,
                        IsPipsOffTime = false
                    };
                    con.eTariffCurrencies.Add(tariffCurrenciesToBeRemoved);
                    con.SaveChanges();


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultTariffCurrency = con.eTariffCurrencies.FirstOrDefault(x => x.ID == tariffCurrenciesToBeRemovedID);
                    resultTariffCurrency.Should().BeNull();

                    var resultTariffCurrencyToBeSaved = con.eTariffCurrencies.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.MarginOffTime == td.Tariffs[0].Exchanges[0].MarginOffTime);
                    resultTariffCurrencyToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        #endregion

        #region TariffProducts save/update/delete
        [Theory]
        [ClassData(typeof(GetTariffProductsDataSave))]
        public void TariffProductsSaveTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultTariffProducts = con.eTariffProducts.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.SetupCost == td.Tariffs[0].Products[0].SetupCost);
                    resultTariffProducts.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetTariffProductsDataSave))]
        public void TariffProductsUpdateTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();
                    // add new
                    service.TariffsData_Save(td);
                    // get the id of the last added
                    var newTariffProductID = con.eTariffProducts.Max(x => x.ID);
                    td.Tariffs[0].Products[0].ID = newTariffProductID;
                    // great new value of the field
                    td.Tariffs[0].Products[0].SetupCost = 999M;


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultTariffProducts = con.eTariffProducts.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.SetupCost == td.Tariffs[0].Products[0].SetupCost);
                    resultTariffProducts.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }


        [Theory]
        [ClassData(typeof(GetTariffProductsDataSave))]
        public void TariffProductsDeleteTest(TariffsData td)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffsService>();
                    int TariffProductToBeRemovedID = con.eTariffProducts.Max(x => x.ID) + 1;

                    eTariffProduct productToBeRemoved = new eTariffProduct()
                    {
                        ID = TariffProductToBeRemovedID,
                        TariffID = 1,
                        CurrencyID = 3,
                        ClientID = null,
                        ProductID = 2,
                        SetupCost = 5M,
                        TrialIntervalCost = 0M,
                        TrialIntervalLength = 0,
                        TrialIntervalType = 0,
                        RegularIntervalCost = 25M,
                        RegularIntervalType = 0,
                        MaxTrialTicks = 0,
                        RegularIntervalLength = 1,
                        SetupCostText = "SetupCost comment 3",
                        IntervalCostText = "IntervalCost comment 3",
                        TrialCostText = String.Empty,
                        ProductOrderID = null
                    };

                    con.eTariffProducts.Add(productToBeRemoved);
                    con.SaveChanges();


                    // act
                    service.TariffsData_Save(td);


                    // assert
                    var resultProductToBeRemoved = con.eTariffProducts.FirstOrDefault(x => x.ID == TariffProductToBeRemovedID);
                    resultProductToBeRemoved.Should().BeNull();

                    var resultProductToBeSaved = con.eTariffProducts.FirstOrDefault(x => x.TariffID == td.Tariffs[0].ID && x.SetupCost == td.Tariffs[0].Products[0].SetupCost);
                    resultProductToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }
        #endregion

        #region ExchangeRate_Get_For_Client
        // Eur to Rub
        [Theory]
        [InlineData(1, 5, 100, 0, 979978461, true, true, 15)]       // Eur to Rub
        [InlineData(1, 5, 50, 0, 979978461, true, true, 20)]        // Eur to Rub
        [InlineData(1, 4, 100, 0, 979978461, true, true, 17.22)]    // Eur to All 
        [InlineData(1, 4, 350, 0, 979978461, true, true, 2)]        // Eur to All 
        [InlineData(2, 4, 125, 0, 979978461, true, true, 7.5)]      // All to All (with amountBC conversion)
        [InlineData(2, 4, 250, 0, 979978461, true, true, 7)]        // All to All (with amountBC conversion)

        public void ExchangeRate_Get_For_Client_Test(short currencyFromID, short currencyToID, decimal amount, decimal baseRate, int? clientID, bool isIncreaseRate, bool isWorkingTime, decimal fee)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                // arrange
                var fix = fixture.fix;
                fix.Inject<PaymentSystemEntitiesSecured>(con);
                var service = fix.Create<TariffsService>();
                var helper = fix.Create<TariffHelper>();


                // act
                (decimal ClientRate, decimal Fee, bool IsPips) exchangeRateGetForClient = helper.ExchangeRate_Get_For_Client(currencyFromID, currencyToID, amount, baseRate, clientID, isIncreaseRate, isWorkingTime);


                // assert
                exchangeRateGetForClient.Fee.Should().Be(fee);
            }
        }

        #endregion

    }
}

