using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Dsl;
using AutoFixture.Kernel;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using PaymentSystem.Engine.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentSystem.Engine.Core.UnitTests
{
    public class SQLiteFixtureBase
       : IDisposable
    {
        private readonly SqliteConnection connection;
        public DbContextOptions<PaymentSystemEntities> Options { get; set; }
        public IFixture fix
        {
            get
            {
                var fx = new Fixture().Customize(new AutoMoqCustomization());
                SetupFixture(fx);
                return fx;
            }
        }

        public SQLiteFixtureBase(bool foreignKeys = false)
        {
            connection = new SqliteConnection($"DataSource=:memory:;Foreign Keys = {foreignKeys}");
            connection.Open();
            Options = new DbContextOptionsBuilder<PaymentSystemEntities>()
                .UseSqlite(connection)
                .Options;

            using (var con = new PaymentSystemEntitiesSecured(Options))
            {
                con.Database.EnsureCreated();
                SeedData(con);
                con.SaveChanges();
            }
        }

        public virtual void SeedData(PaymentSystemEntitiesSecured con)
        {
            
        }

        public virtual void SetupFixture(IFixture fix)
        {

        }

        public void Dispose()
        {
            connection.Dispose();
        }
    }

}

