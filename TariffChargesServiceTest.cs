using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.Data.SqlClient;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using PaymentSystem.Engine.Common.AppArchitectureModel.Currencies;
using PaymentSystem.Engine.Common.AppArchitectureModel.Infrastructure.Exceptions;
using PaymentSystem.Engine.Common.DataContracts.Currencies;
using PaymentSystem.Engine.Common.DataContracts.ServiceProviders;
using PaymentSystem.Engine.Common.Models;
using PaymentSystem.Engine.Core.AppArchitecture;
using PaymentSystem.Engine.Core.AppArchitecture.Tariffs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Xunit;
using System.Linq;

namespace PaymentSystem.Engine.Core.UnitTests.Tariffs
{
    public class SQLLite_TariffHelperTariffsDataSaveProducts__Tests_Fixture : IDisposable
    {
        public readonly SqliteConnection connection;

        public IFixture fix { get; set; }

        public DbContextOptions<PaymentSystemEntities> Options { get; set; }

        public SQLLite_TariffHelperTariffsDataSaveProducts__Tests_Fixture()
        {
            connection = new SqliteConnection("DataSource=:memory:;Foreign Keys = False");
            connection.Open();
            Options = new DbContextOptionsBuilder<PaymentSystemEntities>()
                .UseSqlite(connection).UseLazyLoadingProxies()
                .Options;

            using (var con = new PaymentSystemEntitiesSecured(Options))
            {
                con.Database.EnsureCreated();

                con.eTariffs.Add(new eTariff
                {
                    ID = 1,
                    CanBeProductsChargedDifferentCurrency = true,
                    CanBeChargesChargedDifferentCurrency = true,
                });

                con.eSubAccounts.Add(new eSubAccount { AccountID = 1, CurrencyID = 1, Balance = 100M }); // EUR
                con.eSubAccounts.Add(new eSubAccount { AccountID = 2, CurrencyID = 2, Balance = 500M }); // USD
                con.eSubAccounts.Add(new eSubAccount { AccountID = 3, CurrencyID = 3, Balance = 1000M }); // GBP

                con.SaveChanges();
            }

            fix = new Fixture().Customize(new AutoMoqCustomization());

            var mockCurrenciesService = fix.Freeze<Mock<ICurrenciesService>>();
            Dictionary<short, Currency> Currencies = new Dictionary<short, Currency>()
                {
                    {1, new Currency() { ID = 1, CurrCodeInt = "978", CurrCodeChr = "EUR"} },
                    {2, new Currency() { ID = 2, CurrCodeInt = "840", CurrCodeChr = "USD"} },
                    {3, new Currency() { ID = 3, CurrCodeInt = "826", CurrCodeChr = "GBP"} },
                };
            mockCurrenciesService.Setup(x => x.Currencies).Returns(Currencies);

            var mockCurrencyRatesService = fix.Freeze<Mock<ICurrencyRatesService>>();
            //mockCurrencyRatesService.Setup(x => x.GetExchangeRate_Base(1, 2, true)).Returns(1.1234M); // instead this code we may use like that below
            mockCurrencyRatesService.Setup(x => x.GetExchangeRate_Base(It.IsAny<short>(), It.IsAny<short>(), It.IsAny<bool>())).Returns(1.1234M);
        }

        public void Dispose()
        {
            connection.Dispose();
        }
    }


    public class TariffChargesServiceTest : IClassFixture<SQLLite_TariffHelperTariffsDataSaveProducts__Tests_Fixture>
    {
        private readonly SQLLite_TariffHelperTariffsDataSaveProducts__Tests_Fixture fixture;

        public TariffChargesServiceTest(SQLLite_TariffHelperTariffsDataSaveProducts__Tests_Fixture fixture)
        {
            this.fixture = fixture;
        }

        #region TestData

        #region AllDataInOneClass_Example
        public class GetChargesTest_EnoughMoneyOnTheSubaccount_TestData_Ex
        {
            private readonly SQLLite_TariffHelperTariffsDataSaveProducts__Tests_Fixture fixture;

            public GetChargesTest_EnoughMoneyOnTheSubaccount_TestData_Ex(SQLLite_TariffHelperTariffsDataSaveProducts__Tests_Fixture fixture)
            {
                this.fixture = fixture;
            }
            public Dictionary<string, object> GetTestData()
            {
                using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
                {
                    var subAccounts = con.eSubAccounts.ToList<eSubAccount>();

                    return new Dictionary<string, object>
                    {
                        {
                            "eSP",
                            new eSystemPayment()
                            {
                                AccountID = 1,
                                ProductID = 1,
                                ChargesTypeID = 1,
                                Client = new eClient()
                                {
                                    ID = 1,
                                    TariffID = 1
                                }
                            }
                        },
                        {"amount", 300M },
                        {"currencyID", (short)1},
                        {"comment", "test comment" },
                        {"charges", new List<(int accountID, short currencyID, decimal amount, bool isPending)>() },
                        {"accounts", new List<int>() },
                        {"subAccounts", subAccounts }
                    };
                }

            }
        }
        #endregion

        #region GetChargesTest_EnoughMoneyOnTheSubaccount_TestData
        public class GetChargesTest_EnoughMoneyOnTheSubaccount_TestData : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    new eSystemPayment()
                    {
                        AccountID = 1,
                        ProductID = 1,
                        ChargesTypeID = 1,
                        Client = new eClient()
                        {
                            ID = 1,
                            TariffID = 1
                        }
                    },
                    300M,
                    1,
                    "test comment",
                    new List<(int accountID, short currencyID, decimal amount, bool isPending)>(),
                    new List<int>()
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
        #endregion

        #region GetChargesTest_NotEnoughMoney_TestData
        public class GetChargesTest_NotEnoughMoney_TestData : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    new eSystemPayment()
                    {
                        AccountID = 1,
                        ProductID = 1,
                        ChargesTypeID = 1,
                        Client = new eClient()
                        {
                            ID = 1,
                            TariffID = 1
                        }
                    },
                    3000M,
                    1,
                    "test comment",
                    new List<(int accountID, short currencyID, decimal amount, bool isPending)>(),
                    new List<int>()
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
        #endregion

        #region GetChargesTest_EnoughMoneyOnMainAccount_TestData
        public class GetChargesTest_EnoughMoneyOnMainAccount_TestData : IEnumerable<object[]>
        {
            private readonly SQLLite_TariffHelperTariffsDataSaveProducts__Tests_Fixture fixture;

            public GetChargesTest_EnoughMoneyOnMainAccount_TestData()
            {
            }

            private object[] GetTestData()
            {
                return new object[]
                {
                    new eSystemPayment()
                    {
                        AccountID = 1,
                        ProductID = 1,
                        ChargesTypeID = 1,
                        Client = new eClient()
                        {
                            ID = 1,
                            TariffID = 1
                        }
                    },
                    50M,
                    1,
                    "test comment",
                    new List<(int accountID, short currencyID, decimal amount, bool isPending)>(),
                    new List<int>()
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
        #endregion

        #endregion


        #region Tests

        #region GetChargesTest_EnoughMoneyOnTheSubaccount 
        [Theory]
        [ClassData(typeof(GetChargesTest_EnoughMoneyOnTheSubaccount_TestData))]
        public void GetChargesTest_EnoughMoneyOnTheSubaccount(eSystemPayment eSP, decimal amount, short currencyID, string comment, List<(int accountID, short currencyID, decimal amount, bool isPending)> charges, List<int> accounts)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                // arrange
                var fix = fixture.fix;
                fix.Inject<PaymentSystemEntitiesSecured>(con);
                var service = fix.Create<TariffChargesService>();
                var currenciesService = fix.Create<ICurrenciesService>();

                var subAccounts = con.eSubAccounts.ToList<eSubAccount>();

                string checkComment = comment;
                var checkCurrency = currenciesService.Currencies[currencyID].CurrCodeChr;


                // act
                service.GetCharges(eSP, amount, currencyID, ref checkComment, in charges, in accounts, in subAccounts);


                // assert
                bool isContains = checkComment.Contains(checkCurrency);
                checkComment.Contains(checkCurrency).Should().BeTrue();
                charges.Count.Should().BeGreaterThan(0);
                charges[0].isPending.Should().BeFalse();
            }
        }
        #endregion

        #region GetChargesTest_NotEnoughMoney
        [Theory]
        [ClassData(typeof(GetChargesTest_NotEnoughMoney_TestData))]
        public void GetChargesTest_NotEnoughMoney(eSystemPayment eSP, decimal amount, short currencyID, string comment, List<(int accountID, short currencyID, decimal amount, bool isPending)> charges, List<int> accounts/*, List<eSubAccount> subAccounts*/)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                // arrange
                var fix = fixture.fix;
                fix.Inject<PaymentSystemEntitiesSecured>(con);
                var service = fix.Create<TariffChargesService>();
                var currenciesService = fix.Create<ICurrenciesService>();

                var subAccounts = con.eSubAccounts.ToList<eSubAccount>();

                var checkComment = comment;
                var checkCurrency = currenciesService.Currencies[currencyID].CurrCodeChr;


                // act
                service.GetCharges(eSP, amount, currencyID, ref checkComment, in charges, in accounts, in subAccounts);


                // assert
                charges[0].isPending.Should().BeTrue();
            }
        }
        #endregion

        #region GetChargesTest_EnoughMoneyOnMainAccount
        [Theory]
        [ClassData(typeof(GetChargesTest_EnoughMoneyOnMainAccount_TestData))]
        public void GetChargesTest_EnoughMoneyOnMainAccount(eSystemPayment eSP, decimal amount, short currencyID, string comment, List<(int accountID, short currencyID, decimal amount, bool isPending)> charges, List<int> accounts/*, List<eSubAccount> subAccounts*/)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                // arrange
                var fix = fixture.fix;
                fix.Inject<PaymentSystemEntitiesSecured>(con);
                var service = fix.Create<TariffChargesService>();
                var currenciesService = fix.Create<ICurrenciesService>();

                var subAccounts = con.eSubAccounts.ToList<eSubAccount>();

                var checkComment = comment;
                var checkCurrency = currenciesService.Currencies[currencyID].CurrCodeChr;


                // act
                service.GetCharges(eSP, amount, currencyID, ref checkComment, in charges, in accounts, in subAccounts);


                // assert
                checkComment.Should().Be(comment);
                charges.Count.Should().BeGreaterThan(0);
                charges[0].isPending.Should().BeFalse();
            }
        }
        #endregion

        #endregion
    }
}

