using AutoFixture;
using FluentAssertions;
using Moq;
using PaymentSystem.Engine.Common.AppArchitectureModel.Clients;
using PaymentSystem.Engine.Common.AppArchitectureModel.Currencies;
using PaymentSystem.Engine.Common.AppArchitectureModel.FinanceOperations;
using PaymentSystem.Engine.Common.AppArchitectureModel.Infrastructure.Exceptions;
using PaymentSystem.Engine.Common.AppArchitectureModel.RedisPublic;
using PaymentSystem.Engine.Common.AppArchitectureModel.ServiceProviders;
using PaymentSystem.Engine.Common.AppArchitectureModel.Settings;
using PaymentSystem.Engine.Common.AppArchitectureModel.Tariffs;
using PaymentSystem.Engine.Common.DataContracts.Client;
using PaymentSystem.Engine.Common.DataContracts.Currencies;
using PaymentSystem.Engine.Common.DataContracts.OperationPatterns;
using PaymentSystem.Engine.Common.DataContracts.ServiceProviders;
using PaymentSystem.Engine.Common.DataContracts.Tariffs;
using PaymentSystem.Engine.Common.Models;
using PaymentSystem.Engine.Core.AppArchitecture;
using PaymentSystem.Engine.Core.AppArchitecture.Tariffs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace PaymentSystem.Engine.Core.UnitTests.Tariffs
{
    public class SQLLite_TariffExceptionsService_PersonalTariffsTests_Fixture : SQLiteFixtureBase
    {
        public override void SeedData(PaymentSystemEntitiesSecured con)
        {
            #region con.UnSecured.eClients.Add
            con.UnSecured.eClients.Add(new eClient
            {
                ID = 101334627
            });
            #endregion

            #region con.eProductImplementations.Add
            // eProductImplementations
            con.eProductImplementations.Add(new eProductImplementation
            {
                ID = 2
            });
            con.eProductImplementations.Add(new eProductImplementation
            {
                ID = 3
            });
            #endregion

        }

        public class GetITariffExceptionsServiceData : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    ("val1", "val2"),
                    ("val3", "val4")
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        public override void SetupFixture(IFixture fix)
        {

            // Mock ICurrenciesService
            var mockCurrenciesService = fix.Freeze<Mock<ICurrenciesService>>();
            Dictionary<short, Currency> Currencies = new Dictionary<short, Currency>()
            {
                {1, new Currency() { ID = 1, CurrCodeInt = "978", CurrCodeChr = "EUR"} },
                {2, new Currency() { ID = 2, CurrCodeInt = "840", CurrCodeChr = "USD"} },
                {3, new Currency() { ID = 3, CurrCodeInt = "826", CurrCodeChr = "GBP"} },
            };
            mockCurrenciesService.Setup(x => x.Currencies).Returns(Currencies);

            // Mock IClientInfoService
            var clientShortInformation = new ClientShortInformation();
            var mockClientInfoService = fix.Freeze<Mock<IClientInfoService>>();
            mockClientInfoService.Setup(x => x.GetClientShortInformation(It.IsAny<int>())).Returns(clientShortInformation);

            // Mock ITariffSettingsService
            var mockTariffSettingsService = fix.Freeze<Mock<ITariffSettingsService>>();
            List<TariffChargesType> tariffSettingsService = new List<TariffChargesType>
            {
                new TariffChargesType { ID = 1},
                new TariffChargesType { ID = 2},
                new TariffChargesType { ID = 3}
            };
            mockTariffSettingsService.Setup(x => x.TariffChargesTypes_GetAll()).Returns(tariffSettingsService);

            // Mock ITariffSettingsService
            var mockOperationPatternsService = fix.Freeze<Mock<IOperationPatternsService>>();
            Dictionary<short, OperationPatternFull> Patterns = new Dictionary<short, OperationPatternFull>()
            {
                {1, new OperationPatternFull { } },
                {2, new OperationPatternFull { } },
                {3, new OperationPatternFull { } },
                {4, new OperationPatternFull { } },
            };
            mockOperationPatternsService.Setup(x => x.Patterns).Returns(Patterns);
        }

    }

    public class TariffExceptionsService_PersonalTariffsTests : IClassFixture<SQLLite_TariffExceptionsService_PersonalTariffsTests_Fixture>
    {
        private readonly SQLLite_TariffExceptionsService_PersonalTariffsTests_Fixture fixture;

        public TariffExceptionsService_PersonalTariffsTests(SQLLite_TariffExceptionsService_PersonalTariffsTests_Fixture fixture)
        {
            this.fixture = fixture;
        }

        // DATA

        #region PersonalTariffRemittanceTransactionsDataSave (for save/update/delete tests)
        public class GetPersonalTariffRemittanceTransactionsDataSave : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    new List<TariffExceptions>
                    {
                        new TariffExceptions
                        {
                            Client = new Common.DataContracts.Client.ClientShortInformation
                            {
                                ClientID = 101334627
                            },
                            Commissions = new List<Common.DataContracts.MT.MT_Commission>
                            {
                                new Common.DataContracts.MT.MT_Commission
                                {
                                    ClientID = 101334627,
                                    CountriesKeyword = null,
                                    CountriesKeywordID = 6,
                                    CountryID = null,
                                    Created = DateTime.MinValue, 
                                    Currency = null,
                                    CurrencyID = 1,
                                    Fix = 10M,
                                    From = 100M,
                                    ID = 0,
                                    ISOCountry = null,
                                    IsDeleted = false,
                                    IsFromPersonalTariff = false,
                                    MT_PaymentSystem = null,
                                    MT_PaymentSystemID = 2,
                                    Max = 7M,
                                    Min = 1M,
                                    PaymentType = PaymentSystem.Engine.Common.DataContracts.Enums.MTPaymentTypes.LOCAL_PICK_UP,
                                    PercentValue = 2M,
                                    TariffID = null,
                                    Updated = null
                                }
                            }
                        }
                    }
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        #endregion

        #region PersonalTariffBankTransactionsDataSave (for save/update/delete tests)
        public class GetPersonalTariffBankTransactionsDataSave : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    new List<TariffExceptions>
                    {
                        new TariffExceptions
                        {
                            Client = new Common.DataContracts.Client.ClientShortInformation
                            {
                                ClientID = 101334627
                            },
                            BankCurrencies = new List<TariffBankCurrency>
                            {
                                new TariffBankCurrency
                                {
                                    CurrencyID = 2,
                                    ID = 0,
                                    IncomingAsCurrencyID = null,
                                    Inward = new List<TariffBankSetting>
                                    {
                                        new TariffBankSetting
                                        {
                                            BusinessCategoryID = null,
                                            Charges = null,
                                            ClientID = null,
                                            CurrencyID = 2, 
                                            Fix = 0M,
                                            From = 0M,
                                            ID = 0,
                                            IsFromPersonalTariff = true,
                                            IsIncoming = true,
                                            Max = 0M, 
                                            Min = 0,
                                            Percent = 1,
                                            Priority = PaymentSystem.Engine.Common.DataContracts.Enums.BankWirePriority.SWIFT,
                                            TariffBankCurrencyID = null, 
                                            WireTransferSystemID = 2
                                        }
                                    },
                                    IsActive = false,
                                    IsFromPersonalTariff = true,
                                    OutgoingAsCurrencyID = null,
                                    Outward = new List<TariffBankSetting>
                                    {
                                        new TariffBankSetting
                                        {
                                            BusinessCategoryID = null,
                                            Charges = PaymentSystem.Engine.Common.DataContracts.Enums.BankWireChargeTypes.OUR,
                                            ClientID = null,
                                            CurrencyID = 2,
                                            Fix = 8M,
                                            From = 60M,
                                            ID = 0,
                                            IsFromPersonalTariff = false,
                                            IsIncoming = false,
                                            Max = 10M,
                                            Min = 1M,
                                            Percent = 5M,
                                            Priority = PaymentSystem.Engine.Common.DataContracts.Enums.BankWirePriority.SWIFT,
                                            TariffBankCurrencyID = null,
                                            WireTransferSystemID = 6
                                        }
                                    },
                                    SHACountries = null,
                                    TariffID = 0
                                }
                            }
                        }
                    }
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        #endregion

        #region PersonalTariffGeneralTransactionsDataSave (for save/update/delete tests)
        public class GetPersonalTariffGeneralTransactionsDataSave : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    new List<TariffExceptions>
                    {
                        new TariffExceptions
                        {
                            Client = new Common.DataContracts.Client.ClientShortInformation
                            {
                                ClientID = 101334627
                            },
                            SettingsCurrencies = new List<TariffSettingsCurrency>
                            {
                                new TariffSettingsCurrency
                                {
                                    CurrencyID = 1,
                                    Settings = new List<TariffSetting>
                                    {
                                        new TariffSetting
                                        {
                                            BusinessCategoryID = null,
                                            ClientID = 101334627,
                                            CurrencyID = null,
                                            DiscountPercent = 0M,
                                            Fix = 3M,
                                            ID = 0,
                                            IncomingFix = 0M,
                                            IncomingMax = 0M,
                                            IncomingMin = 0M,
                                            IncomingPercent = 0M,
                                            IsFromPersonalTariff = false,
                                            Level = 0,
                                            Max = 9M,
                                            Min = 2M,
                                            OperationPatternID = 4,
                                            Percent = 7M,
                                            TariffID = null,
                                            WithIncoming = false
                                        }
                                    }
                                }
                            }
                        }
                    }
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        #endregion

        #region PersonalTariffChargesDataSave (for save/update/delete tests)
        public class GetPersonalTariffChargesDataSave : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    new List<TariffExceptions>
                    {
                        new TariffExceptions
                        {
                            Client = new Common.DataContracts.Client.ClientShortInformation
                            {
                                ClientID = 101334627
                            },
                            Charges = new List<TariffCharge>
                            {
                                new TariffCharge
                                {
                                    BusinessCategoryID = null, 
                                    ChargesTypeID = 1,
                                    ClientID = 101334627,
                                    CurrencyID = 1,
                                    Fix = 20M,
                                    ID = 0,
                                    IsDefaultCurrency = false,
                                }
                            }
                        }
                    }
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        #endregion

        #region PersonalTariffProductsDataSave (for save/update/delete tests)
        public class GetPersonalTariffProductsDataSave : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    new List<TariffExceptions>
                    {
                        new TariffExceptions
                        {
                            Client = new Common.DataContracts.Client.ClientShortInformation
                            {
                                ClientID = 101334627
                            },
                            Products = new List<TariffProduct>
                            {
                                new TariffProduct
                                {
                                    ClientID = null,
                                    CurrencyID = 1,
                                    ID = 0,
                                    IntervalCostText = "dddd",
                                    IsFromPersonalTariff = false,
                                    MaxTrialTicks = 0,
                                    ProductID = 3,
                                    ProductOrderID = null,
                                    RegularIntervalCost = 6M, 
                                    RegularIntervalLength = 2,
                                    RegularIntervalType = PaymentSystem.Engine.Common.DataContracts.Subscriptions.IntervalTypes.Days,
                                    SetupCost = 2M,
                                    SetupCostText = "setup test",
                                    TariffID = null, 
                                    TrialCostText = "ssss",
                                    TrialIntervalCost = 3M,
                                    TrialIntervalLength = 0,
                                    TrialIntervalType = PaymentSystem.Engine.Common.DataContracts.Subscriptions.IntervalTypes.Days
                                }
                            }
                        }
                    }
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        #endregion

        #region PersonalTariffExchangesDataSave (for save/update/delete tests)
        public class GetPersonalTariffExchangesDataSave : IEnumerable<object[]>
        {
            private object[] GetTestData()
            {
                return new object[]
                {
                    new List<TariffExceptions>
                    {
                        new TariffExceptions
                        {
                            Client = new Common.DataContracts.Client.ClientShortInformation
                            {
                                ClientID = 101334627
                            },
                            Exchanges = new List<TariffCurrency>
                            {
                                new TariffCurrency
                                {
                                    AmountFrom = 0M,
                                    ClientID = null,
                                    CurrencyFromID = 1,
                                    CurrencyToID = 2,
                                    //ID = 152,
                                    IsFromPersonalTariff = true,
                                    IsPips = false,
                                    IsPipsOffTime = false,
                                    Margin = 2M,
                                    MarginOffTime = 0M,
                                    TariffID = null
                                },
                                new TariffCurrency
                                {
                                    AmountFrom = 200M,
                                    ClientID = null,
                                    CurrencyFromID = 1,
                                    CurrencyToID = 2,
                                    //ID = 152,
                                    IsFromPersonalTariff = true,
                                    IsPips = false,
                                    IsPipsOffTime = false,
                                    Margin = 10M,
                                    MarginOffTime = 0M,
                                    TariffID = null
                                }
                            }
                        }
                    }
                };
            }
            public IEnumerator<object[]> GetEnumerator()
            {
                yield return GetTestData();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        #endregion


        // TESTS

        #region PersonalTariffRemittanceTransactionsTests save/update/delete

        [Theory]
        [ClassData(typeof(GetPersonalTariffRemittanceTransactionsDataSave))]
        public void PersonalTariffRemittanceTransactionsSaveTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultTariffRemittanceTransaction = con.eMT_Commissions.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.Fix == exceptions[0].Commissions[0].Fix && x.CurrencyID == exceptions[0].Commissions[0].CurrencyID);
                    resultTariffRemittanceTransaction.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetPersonalTariffRemittanceTransactionsDataSave))]
        public void PersonalTariffRemittanceTransactionsUpdateTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();
                    // add new
                    service.TariffExceptions_Update(exceptions);
                    // get the id of the last added
                    var TariffRemittanceTransactionID = con.eMT_Commissions.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.Fix == exceptions[0].Commissions[0].Fix).ID;
                    exceptions[0].Commissions[0].ID = TariffRemittanceTransactionID;
                    // great new value of the field
                    exceptions[0].Commissions[0].Fix = 850M;


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultTariffRemittanceTransaction = con.eMT_Commissions.FirstOrDefault(x => x.ID == TariffRemittanceTransactionID && x.ClientID == exceptions[0].Commissions[0].ClientID && x.Fix == exceptions[0].Commissions[0].Fix);
                    resultTariffRemittanceTransaction.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetPersonalTariffRemittanceTransactionsDataSave))]
        public void PersonalTariffRemittanceTransactionsDeleteTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();
                    int tariffRemittanceTransactionToBeRemovedID = con.eMT_Commissions.Count() > 0 ? con.eMT_Commissions.OrderByDescending(x => x.ID).FirstOrDefault().ID + 1 : 1;

                    eMT_Commission tariffRemittanceTransactionToBeRemoved = new eMT_Commission
                    {
                        ClientID = 101334627,
                        CountriesKeyword = null,
                        CountriesKeywordID = 6,
                        CountryID = null,
                        Created = DateTime.MinValue,
                        Currency = null,
                        CurrencyID = 1,
                        Fix = 100M,
                        From = 100M,
                        ID = tariffRemittanceTransactionToBeRemovedID,
                        ISOCountry = null,
                        IsDeleted = false,
                        MT_PaymentSystem = null,
                        MT_PaymentSystemID = 2,
                        Max = 7M,
                        Min = 1M,
                        PaymentType = PaymentSystem.Engine.Common.DataContracts.Enums.MTPaymentTypes.LOCAL_PICK_UP,
                        PercentValue = 2M,
                        TariffID = null,
                        Updated = null
                    };
                    con.eMT_Commissions.Add(tariffRemittanceTransactionToBeRemoved);
                    con.SaveChanges();


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultTariffRemittanceTransactionToBeRemoved = con.eMT_Commissions.FirstOrDefault(x => x.ID == tariffRemittanceTransactionToBeRemovedID && x.Fix == tariffRemittanceTransactionToBeRemoved.Fix && x.IsDeleted == true);
                    resultTariffRemittanceTransactionToBeRemoved.Should().NotBeNull();

                    var resultTariffRemittanceTransactionToBeSaved = con.eMT_Commissions.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.Fix == exceptions[0].Commissions[0].Fix && x.CurrencyID == exceptions[0].Commissions[0].CurrencyID && x.IsDeleted == false);
                    resultTariffRemittanceTransactionToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }
        #endregion

        #region PersonalTariffBankTransactionsTests save/update/delete

        [Theory]
        [ClassData(typeof(GetPersonalTariffBankTransactionsDataSave))]
        public void PersonalTariffBankTransactionsSaveTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultTariffBankTransactionInward = con.eTariffBankSettings.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.IsIncoming == true && x.Fix == exceptions[0].BankCurrencies[0].Inward[0].Fix && x.CurrencyID == exceptions[0].BankCurrencies[0].Inward[0].CurrencyID);
                    resultTariffBankTransactionInward.Should().NotBeNull();

                    var resultTariffBankTransactionOutward = con.eTariffBankSettings.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.IsIncoming == false && x.Fix == exceptions[0].BankCurrencies[0].Outward[0].Fix && x.CurrencyID == exceptions[0].BankCurrencies[0].Outward[0].CurrencyID);
                    resultTariffBankTransactionOutward.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetPersonalTariffBankTransactionsDataSave))]
        public void PersonalTariffBankTransactionsUpdateTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();
                    // add new
                    service.TariffExceptions_Update(exceptions);
                    // get the id of the last added
                    var TariffBankTransactionsInwardID = con.eTariffBankSettings.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.IsIncoming == true && x.Fix == exceptions[0].BankCurrencies[0].Inward[0].Fix).ID;
                    exceptions[0].BankCurrencies[0].Inward[0].ID = TariffBankTransactionsInwardID;
                    var TariffBankTransactionsOutwardID = con.eTariffBankSettings.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.IsIncoming == false && x.Fix == exceptions[0].BankCurrencies[0].Outward[0].Fix).ID;
                    exceptions[0].BankCurrencies[0].Outward[0].ID = TariffBankTransactionsOutwardID;
                    // great new value of the field
                    exceptions[0].BankCurrencies[0].Inward[0].Fix = 750M;
                    exceptions[0].BankCurrencies[0].Outward[0].Fix = 150M;


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultBankTransactionsInward = con.eTariffBankSettings.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.IsIncoming == true && x.Fix == exceptions[0].BankCurrencies[0].Inward[0].Fix && x.ID == TariffBankTransactionsInwardID);
                    resultBankTransactionsInward.Should().NotBeNull();
                    var resultBankTransactionsOutward = con.eTariffBankSettings.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.IsIncoming == false && x.Fix == exceptions[0].BankCurrencies[0].Outward[0].Fix && x.ID == TariffBankTransactionsOutwardID);
                    resultBankTransactionsOutward.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetPersonalTariffBankTransactionsDataSave))]
        public void PersonalTariffBankTransactionsDeleteTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();
                    int tariffBankTransactionToBeRemovedID = con.eTariffBankSettings.Count() > 0 ? con.eTariffBankSettings.OrderByDescending(x => x.ID).FirstOrDefault().ID + 1 : 1;


                    eTariffBankSetting tariffBankTransactionToBeRemoved = new eTariffBankSetting
                    {
                        BusinessCategoryID = null,
                        Charges = null,
                        ClientID = 101334627,
                        CurrencyID = 2,
                        Fix = 100M,
                        From = 50M,
                        ID = tariffBankTransactionToBeRemovedID,
                        IsIncoming = true,
                        Max = 5M,
                        Min = 7,
                        Percent = 1,
                        Priority = PaymentSystem.Engine.Common.DataContracts.Enums.BankWirePriority.SWIFT,
                        TariffBankCurrencyID = null,
                        WireTransferSystemID = 2
                    }
;
                    con.eTariffBankSettings.Add(tariffBankTransactionToBeRemoved);
                    con.SaveChanges();


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultTariffBankTransactionToBeRemoved = con.eTariffBankSettings.FirstOrDefault(x => x.ID == tariffBankTransactionToBeRemovedID && x.Fix == tariffBankTransactionToBeRemoved.Fix);
                    resultTariffBankTransactionToBeRemoved.Should().BeNull();

                    var resultTariffBankTransactionInwardToBeSaved = con.eTariffBankSettings.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.IsIncoming == true && x.Fix == exceptions[0].BankCurrencies[0].Inward[0].Fix && x.CurrencyID == exceptions[0].BankCurrencies[0].Inward[0].CurrencyID);
                    resultTariffBankTransactionInwardToBeSaved.Should().NotBeNull();
                    var resultTariffBankTransactionOutwardToBeSaved = con.eTariffBankSettings.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.IsIncoming == false && x.Fix == exceptions[0].BankCurrencies[0].Outward[0].Fix && x.CurrencyID == exceptions[0].BankCurrencies[0].Outward[0].CurrencyID);
                    resultTariffBankTransactionOutwardToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }
        #endregion

        #region PersonalTariffGeneralTransactionsTests save/update/delete

        [Theory]
        [ClassData(typeof(GetPersonalTariffGeneralTransactionsDataSave))]
        public void PersonalTariffGeneralTransactionsSaveTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultTariffCharge = con.eTariffSettings.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.Fix == exceptions[0].SettingsCurrencies[0].Settings[0].Fix && x.CurrencyID == exceptions[0].SettingsCurrencies[0].CurrencyID);
                    resultTariffCharge.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetPersonalTariffGeneralTransactionsDataSave))]
        public void PersonalTariffGeneralTransactionsUpdateTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();
                    // add new
                    service.TariffExceptions_Update(exceptions);
                    // get the id of the last added
                    var TariffGeneralTransactionsID = con.eTariffSettings.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.Fix == exceptions[0].SettingsCurrencies[0].Settings[0].Fix).ID;
                    exceptions[0].SettingsCurrencies[0].Settings[0].ID = TariffGeneralTransactionsID;
                    // great new value of the field
                    exceptions[0].SettingsCurrencies[0].Settings[0].Fix = 550M;


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultTariffCurrency = con.eTariffSettings.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.Fix == exceptions[0].SettingsCurrencies[0].Settings[0].Fix && x.ID == TariffGeneralTransactionsID);
                    resultTariffCurrency.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetPersonalTariffGeneralTransactionsDataSave))]
        public void PersonalTariffGeneralTransactionsDeleteTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();
                    int tariffGeneralTransactionsToBeRemovedID = con.eTariffSettings.Count() > 0 ? con.eTariffSettings.OrderByDescending(x => x.ID).FirstOrDefault().ID + 1 : 1;

                    eTariffSetting tariffGeneralTransactionToBeRemoved = new eTariffSetting
                    {
                        BusinessCategoryID = null,
                        ClientID = 101334627,
                        CurrencyID = null,
                        DiscountPercent = 0M,
                        Fix = 30M,
                        ID = tariffGeneralTransactionsToBeRemovedID,
                        IncomingFix = 0M,
                        IncomingMax = 0M,
                        IncomingMin = 0M,
                        IncomingPercent = 0M,
                        Max = 3M,
                        Min = 4M,
                        OperationPatternID = 3,
                        Percent = 5M,
                        TariffID = null,
                        WithIncoming = false
                    };
                    con.eTariffSettings.Add(tariffGeneralTransactionToBeRemoved);
                    con.SaveChanges();


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultTariffGeneralTransactionToBeRemoved = con.eTariffSettings.FirstOrDefault(x => x.ID == tariffGeneralTransactionsToBeRemovedID && x.Fix == tariffGeneralTransactionToBeRemoved.Fix);
                    resultTariffGeneralTransactionToBeRemoved.Should().BeNull();

                    var resultTariffGeneralTransactionToBeSaved = con.eTariffSettings.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.Fix == exceptions[0].SettingsCurrencies[0].Settings[0].Fix && x.CurrencyID == exceptions[0].SettingsCurrencies[0].CurrencyID);
                    resultTariffGeneralTransactionToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }
        #endregion

        #region PersonalTariffChargesTests save/update/delete

        [Theory]
        [ClassData(typeof(GetPersonalTariffChargesDataSave))]
        public void PersonalTariffChargesSaveTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultTariffCharge = con.eTariffCharges.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.Fix == exceptions[0].Charges[0].Fix && x.CurrencyID == exceptions[0].Charges[0].CurrencyID);
                    resultTariffCharge.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetPersonalTariffChargesDataSave))]
        public void PersonalTariffChargesUpdateTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();
                    // add new
                    service.TariffExceptions_Update(exceptions);
                    // get the id of the last added
                    var TariffChargeID = con.eTariffCharges.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.Fix == exceptions[0].Charges[0].Fix).ID;
                    exceptions[0].Charges[0].ID = TariffChargeID;
                    // great new value of the field
                    exceptions[0].Charges[0].Fix = 250M;


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultTariffCurrency = con.eTariffCharges.FirstOrDefault(x => x.ID == TariffChargeID && x.ClientID == exceptions[0].Charges[0].ClientID && x.Fix == exceptions[0].Charges[0].Fix);
                    resultTariffCurrency.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetPersonalTariffChargesDataSave))]
        public void PersonalTariffChargesDeleteTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();
                    int tariffChargesToBeRemovedID = con.eTariffCharges.Count() > 0 ? con.eTariffCharges.OrderByDescending(x => x.ID).FirstOrDefault().ID + 1 : 1;

                    eTariffCharge tariffChargeToBeRemoved = new eTariffCharge
                    {
                        BusinessCategoryID = null,
                        ChargesTypeID = 1,
                        ClientID = 101334627,
                        CurrencyID = 1,
                        Fix = 200M,
                        ID = tariffChargesToBeRemovedID,
                        IsDefaultCurrency = false,
                    };
                    con.eTariffCharges.Add(tariffChargeToBeRemoved);
                    con.SaveChanges();


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultTariffChargeToBeRemoved = con.eTariffCharges.FirstOrDefault(x => x.ID == tariffChargesToBeRemovedID && x.Fix == tariffChargeToBeRemoved.Fix);
                    resultTariffChargeToBeRemoved.Should().BeNull();

                    var resultTariffChargeToBeSaved = con.eTariffCharges.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.Fix == exceptions[0].Charges[0].Fix && x.CurrencyID == exceptions[0].Charges[0].CurrencyID);
                    resultTariffChargeToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }
        #endregion

        #region PersonalTariffProductsTest save/update/delete/exception

        [Theory]
        [ClassData(typeof(GetPersonalTariffProductsDataSave))]
        public void PersonalTariff_ProductAlreadyExistsForThisTariffExceptionTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();
                    service.TariffExceptions_Update(exceptions);

                    // act
                    CommonException exception = Assert.Throws<CommonException>(() => service.TariffExceptions_Update(exceptions));

                    // assert
                    exception.Message.Should().StartWith("Such product already exists for this tariff");

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetPersonalTariffProductsDataSave))]
        public void PersonalTariffProductsSaveTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultTariffCurrency_0 = con.eTariffProducts.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.RegularIntervalCost == exceptions[0].Products[0].RegularIntervalCost && x.SetupCostText == exceptions[0].Products[0].SetupCostText);
                    resultTariffCurrency_0.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetPersonalTariffProductsDataSave))]
        public void PersonalTariffProductsUpdateTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();
                    // add new
                    service.TariffExceptions_Update(exceptions);
                    // get the id of the last added
                    var TariffProductID = con.eTariffProducts.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.SetupCostText == exceptions[0].Products[0].SetupCostText).ID;
                    exceptions[0].Products[0].ID = TariffProductID;
                    // great new value of the field
                    exceptions[0].Products[0].SetupCostText = "new SetupCostText";


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultTariffCurrency = con.eTariffProducts.FirstOrDefault(x => x.ID == TariffProductID && x.ClientID == exceptions[0].Products[0].ClientID && x.SetupCostText == exceptions[0].Products[0].SetupCostText);
                    resultTariffCurrency.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetPersonalTariffProductsDataSave))]
        public void PersonalTariffProductDeleteTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();
                    int tariffProductsToBeRemovedID = con.eTariffProducts.Count() > 0 ? con.eTariffProducts.OrderByDescending(x => x.ID).FirstOrDefault().ID + 1 : 1;

                    eTariffProduct tariffProductToBeRemoved = new eTariffProduct
                    {
                        ID = tariffProductsToBeRemovedID,
                        ClientID = 101334627,
                        CurrencyID = 1,
                        IntervalCostText = "dddd",
                        MaxTrialTicks = 0,
                        ProductID = 2,
                        ProductOrderID = null,
                        RegularIntervalCost = 6M,
                        RegularIntervalLength = 2,
                        RegularIntervalType = PaymentSystem.Engine.Common.DataContracts.Subscriptions.IntervalTypes.Days,
                        SetupCost = 2M,
                        SetupCostText = "tariffProductToBeRemoved setup test",
                        TariffID = null,
                        TrialCostText = "ddd",
                        TrialIntervalCost = 3M,
                        TrialIntervalLength = 0,
                        TrialIntervalType = PaymentSystem.Engine.Common.DataContracts.Subscriptions.IntervalTypes.Days
                    };
                    con.eTariffProducts.Add(tariffProductToBeRemoved);
                    con.SaveChanges();


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resulttariffProductsToBeRemoved = con.eTariffProducts.FirstOrDefault(x => x.ID == tariffProductsToBeRemovedID && x.SetupCostText == tariffProductToBeRemoved.SetupCostText);
                    resulttariffProductsToBeRemoved.Should().BeNull();

                    var resultTariffProductsToBeSaved = con.eTariffProducts.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.IntervalCostText == exceptions[0].Products[0].IntervalCostText && x.SetupCostText == exceptions[0].Products[0].SetupCostText);
                    resultTariffProductsToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }
        #endregion

        #region PersonalTariffExchanges save/update/delete
        [Theory]
        [ClassData(typeof(GetPersonalTariffExchangesDataSave))]
        public void PersonalTariffExchangesSaveTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();


                    // act
                    service.TariffExceptions_Update(exceptions);


                    //// assert
                    var resultTariffCurrency_0 = con.eTariffCurrencies.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.Margin == exceptions[0].Exchanges[0].Margin);
                    resultTariffCurrency_0.Should().NotBeNull();

                    var resultTariffCurrency_1 = con.eTariffCurrencies.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.Margin == exceptions[0].Exchanges[1].Margin);
                    resultTariffCurrency_1.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetPersonalTariffExchangesDataSave))]
        public void PersonalTariffExchangesUpdateTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();
                    // add new
                    service.TariffExceptions_Update(exceptions);
                    // get the id of the last added
                    var TariffCurrencyID = con.eTariffCurrencies.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.Margin == exceptions[0].Exchanges[0].Margin).ID;
                    exceptions[0].Exchanges[0].ID = TariffCurrencyID;
                    // great new value of the field
                    exceptions[0].Exchanges[0].MarginOffTime = 100M;


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resultTariffCurrency = con.eTariffCurrencies.FirstOrDefault(x => x.ID == TariffCurrencyID && x.ClientID == exceptions[0].Exchanges[0].ClientID && x.MarginOffTime == exceptions[0].Exchanges[0].MarginOffTime);
                    resultTariffCurrency.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }

        [Theory]
        [ClassData(typeof(GetPersonalTariffExchangesDataSave))]
        public void PersonalTariffExchangesDeleteTest(List<TariffExceptions> exceptions)
        {
            using (var con = new PaymentSystemEntitiesSecured(fixture.Options))
            {
                using (var tran = con.Database.BeginTransaction())
                {
                    // arrange
                    var fix = fixture.fix;
                    fix.Inject<PaymentSystemEntitiesSecured>(con);
                    var service = fix.Create<TariffExceptionsService>();
                    int tariffCurrenciesToBeRemovedID = con.eTariffCurrencies.Count() > 0 ? con.eTariffCurrencies.OrderByDescending(x => x.ID).FirstOrDefault().ID + 1 : 1;

                    eTariffCurrency tariffCurrenciesToBeRemoved = new eTariffCurrency
                    {
                        ID = tariffCurrenciesToBeRemovedID,
                        TariffID = 0,
                        ClientID = 101334627,
                        CurrencyFromID = 1,
                        CurrencyToID = 2,
                        Margin = 1M,
                        MarginOffTime = 2M,
                        AmountFrom = 0M,
                        IsPips = false,
                        IsPipsOffTime = false
                    };
                    con.eTariffCurrencies.Add(tariffCurrenciesToBeRemoved);
                    con.SaveChanges();


                    // act
                    service.TariffExceptions_Update(exceptions);


                    // assert
                    var resulttariffCurrenciesToBeRemoved = con.eTariffCurrencies.FirstOrDefault(x => x.ID == tariffCurrenciesToBeRemovedID);
                    resulttariffCurrenciesToBeRemoved.Should().BeNull();

                    var resultTariffCurrencyToBeSaved = con.eTariffCurrencies.FirstOrDefault(x => x.ClientID == exceptions[0].Client.ClientID && x.Margin == exceptions[0].Exchanges[0].Margin);
                    resultTariffCurrencyToBeSaved.Should().NotBeNull();

                    tran.Rollback();
                }
            }
        }
        #endregion

    }
}

